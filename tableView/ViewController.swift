//
//  ViewController.swift
//  tableView
//
//  Created by PRO13 on 01.09.2020.
//  Copyright © 2020 PRO13. All rights reserved.
//

import UIKit



class ViewController: UIViewController {

    var userModel: UserModel!
    
    
    
    //MARK: - 'Outlets'
    
    @IBOutlet weak var avatarIndiView: UIImageView!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       userModel = UserModel(phone: "+380631234567",
                             firstName: "Anna",
                             lastName: "Knysh",
                             email: "anna@gmail.com")
       configurationView()
    }

    
    func configurationView() {
        phoneField.text = userModel.phone
        nameField.text = userModel.firstName
        lastNameField.text = userModel.lastName
        emailField.text = userModel.email
    }
    
    
    //MARK: - 'Action'
    
    @IBAction func phoneFieldAction(_ sender: UITextField) {
        userModel.phone = sender.text ?? userModel.phone
    }
    
    @IBAction func firstNameFieldAction(_ sender: UITextField) {
        userModel.firstName = sender.text ?? userModel.firstName
        sender.text = userModel.firstName
    }
    
    @IBAction func lastNameFieldAction(_ sender: UITextField) {
        userModel.lastName = sender.text ?? userModel.lastName
    }
    
    @IBAction func emaildAction(_ sender: UITextField) {
        userModel.email = sender.text ?? userModel.email
    }
    
    
}


//MARK: - 'Delegate'

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == phoneField {
            nameField.becomeFirstResponder()
        } else if textField == nameField {
            lastNameField.becomeFirstResponder()
        } else if textField == lastNameField {
            emailField.becomeFirstResponder()
        } else if textField == emailField {
            emailField.becomeFirstResponder()
        }
        
        return true
    }
}
