//
//  Model.swift
//  tableView
//
//  Created by PRO13 on 03.09.2020.
//  Copyright © 2020 PRO13. All rights reserved.
//


class UserModel {
    var avatar: String?
    var phone: String
    var firstName: String
    var lastName: String
    var email: String
    
    init(avatar: String? = nil, phone: String, firstName: String, lastName: String, email: String) {
        self.phone = phone
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
    }
    
    
}
